package com.zuitt.WDC043_S2_A1;

import java.util.Scanner;
public class LeapYear {

    public static void main(String[] args){

        Scanner Myobj =  new Scanner(System.in);

        System.out.println("Enter any year: ");

        int year = Myobj.nextInt();




        if (year % 400 == 0) {

            System.out.println("Year " + year + " is a Leap Year");
        }
        else if (year % 100 == 0) {
            System.out.println("Year " + year + " is not a Leap Year");
        } else if (year % 4 == 0) {
            System.out.println("Year " + year + " is a Leap Year");
        } else {
            System.out.println("Year " + year + " is not a Leap Year");
        }



    }
}
