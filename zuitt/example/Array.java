package com.zuitt.example;

import java.util.Arrays;
import java.util.ArrayList;
import  java.util.HashMap;
public class Array {

    //[SECTION] Java Collections
    // are single unit of objects.
    // useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops.
    public static void main(String[] args) {

        //[SECTION] Array
        // In Java, arrays are containers of values of the same type given a predefined amount of value.
        // Java arrays are more rigid, once the size and sata type are defined, they can no longer be changed.

        // Syntax: Array Declaration
        // dataType[] identifier = new dataType[numOfElements]];
        // "[]" indicates that the data type should be able to hold multiple values.
        // "new" keyword is used for non-primitive data types to tell Java to create the said variable.
        // The values of the array is initialized with 0 or null.


        int[] intArray = new int[5];

        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
//        intArray[5] = 100; - out of bound error.

        // This will return the memory address of the array.
        //System.out.println(intArray);

        //To print the intArray, we need to import the Array Class and use the ".toString()" method.
        //This will convert the array as strings in the console/terminal.
        System.out.println(Arrays.toString(intArray));

        //Syntax: Array Declaration with Initialization
        // dataType[] identifier = {elementA, elementB, elementC, ...};
        // The compiler automatically specifies the size by counting the number of elements in the array.

        String[] names = {"John", "Jane", "Joe"};
//        names[3] = "Jose"; - out of bounds error.
        System.out.println(Arrays.toString(names));

        //Sample Java Array method:
        //Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort method: " + Arrays.toString(intArray));

        //Multidimensional Arrays
        // A two-dimensional array that can be described by two lengths nested within each other just like a matrix.
        //first length is the row, while the second length is the column.

        String[][] classroom = new String[3][3];

        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Jun-jun";
        classroom[1][2] = "Jobert";

        //Third row
        classroom[2][0] = "Micky";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

//        System.out.println(Arrays.toString(classroom));
        // We use deepToString for multidimensional arrays
        System.out.println(Arrays.deepToString(classroom));

        //[SECTION] ArrayLists
        // are resizable arrays, wherein element can be added or removed whenever needed.

        // Syntax:
        //ArrayList<T> identifier = new ArrayList<T>();
        //"<T>" - is used to specify that the list can only have one type of object in a collection.
        // ArrayList cannot hold primitive data types, "java wrapper classes" provide a way to used non-primitive data types/objects.
        // In short, Object version of primitive data types with methods

        //Declaring an ArrayList
        //ArrayList<int> numbers = new ArrayList<int>()- Type argument cannot be of primitive type
        ArrayList<Integer> numbers = new ArrayList<Integer>(); // valid syntax.


        //ArrayList<String> students = new ArrayList<String>();

        //Declaring ArraList with values
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

        //Adding elements
        //arrayListName.add(element)
        students.add("Jordan");
        students.add("Martin");
        System.out.println(students);

        // Accessing Elements
        //
        System.out.println("mahay  " + students.get(0));

        students.set(0, "jock");
        System.out.println(students);


        HashMap<String, String> jobPosition = new HashMap<String, String>(){
            {
                put("Teacher", "John");
                put("Artist", "Jane");
            }
        };
        System.out.println(jobPosition);

        //
        jobPosition.put("Student", "Miya");
        jobPosition.put("Engineer", "Andy");

        System.out.println(jobPosition);
        System.out.println(jobPosition.get("Student"));


        jobPosition.replace("Student", "miya khalifa");
        System.out.println(jobPosition.get("Student"));


        jobPosition.remove("Engineer");
        System.out.println(jobPosition);

        jobPosition.clear();
        System.out.println(jobPosition);














    }
}

